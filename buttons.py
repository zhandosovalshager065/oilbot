from aiogram import types

buttons = ["Замена масла в ДВС", "Замена масла в АКПП", "Замена масла в ГУР", "Замена масла в Мостах", "Промывка инжектора"]

cancel_button = ["Отменить заполнение"]
keyboard = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
keyboard_cancel = types.ReplyKeyboardMarkup(one_time_keyboard=False, resize_keyboard=True)
keyboard.add(*buttons)
keyboard_cancel.add(*cancel_button)
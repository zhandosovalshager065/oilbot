from fpdf import FPDF
from config import *
from buttons import buttons

# def make_pdf(cells: dict):
#     pdf = FPDF(format='letter')
#     pdf.add_page()
#     pdf.add_font('Roboto', '', roboto_font_path, uni=True)
#     pdf.set_font("Roboto", size=12)
#     w, h, border, align = 200, 10, 0, 'A'
#     for k in cells:
#         pdf.multi_cell( w, h, f"{k}: {cells[k]}", border=border, align=align)
#     pdf.image(name=pdf_background_img_path, x = None, y = None, w = 100, h = 0, type = '', link = '')
#     return pdf

def make_pdf(cells: dict):

    # print(cells['Заголовок'])

    # for cell in cells:
    #     a = cells[cell]
    #     print(a)

    pdf = FPDF(format='letter')
    pdf.add_page()
    pdf.image(name=pdf_background2_img_path, x = 0, 
            y = 0, 
            w = 220, 
            h = 280, 
            type = '', 
            link = '') 
    #### PDF HEADER ####

    def pdfHeader():
        pdf.add_font('Roboto-Bold', '', roboto_bold_font_path, uni=True)
        pdf.add_font('Roboto', '', roboto_font_path, uni=True)
        pdf.set_font("Roboto-Bold", size=20)
        pdf.image(name=pdf_logo_img_path, x = None, 
            y = None, 
            w = 30, 
            h = 0, 
            type = '', 
            link = '')

        pdf.cell(190, 20, cells['Заголовок'], 0, 1, 'C', )

        pdf.set_font('Roboto', size=16)
        pdf.cell(190, 20, time_now, 0, 1, 'C', )
    pdfHeader()
    #### PDF HEADER ####

    #### PDF BODY ####
    def pdfBody():
        pdf.set_font("Roboto", size=12)
        w, h, border, align = 100, 10, 0, 'A'
        del cells["Заголовок"]
        # Удалил Заголовок где не нужно
        for k in cells:
            pdf.multi_cell( w, h, f"{k}: {cells[k]}", border=border, align=align)
    
    pdfBody() 
    #### PDF BODY ####

    #### PDF FOOTER ####
    def pdfFooter():
        pdf.set_font("Roboto-Bold", size=16)
        pdf.cell(190, 20, pdf_sto_name, 0, 1, 'C')
        pdf.set_font("Roboto-Bold", size=12)
        pdf.cell(100, 10, 'Адрес:' + ' ' + pdf_sto_location, 0, 1, 'A')
        pdf.cell(100, 10, 'Номер телефона:' + ' ' + pdf_sto_num, 0, 1, 'A')
        pdf.cell(100, 10, 'Инстаграм:' + ' ' + pdf_sto_insta, 0, 1, 'A')
        pdf.cell(100, 10, 'Email:' + ' ' + pdf_sto_email, 0, 1, 'A')
        
    pdfFooter()
    #### PDF FOOTER ####

    return pdf
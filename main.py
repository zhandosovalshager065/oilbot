#!venv/bin/python qweqwe123
from aiogram.utils import executor
from config import dp

from modules.moduledvs import *
from modules.modulegur import *
from modules.moduleakpp import *
from modules.modulemost import *
from modules.moduleinj import *

if __name__ == "__main__":
    # Запуск бота
    executor.start_polling(dp, skip_updates=True)
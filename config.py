import datetime
import logging
import os

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from cf import token

dvs_zamena = 8000 # Замена масла в ДВС каждые 8000км
akpp_zamena = 40000 # Замена масла в АКПП каждые 40000км
gur_zamena = 40000 # Замена масла в ГУР каждые 40000км
most_zamena = 40000 # Замена масла в Мостпх каждые 40000км
inj_promyvka = 40000 # Промывка инжектора каждые 40000км
pdf_sto_location = 'Астана , пр. Магжана Жумабаева 13'
pdf_sto_num = '87781117885'
pdf_sto_name = 'Автосервис Жетiсу'
pdf_sto_insta = '@zhetisu.pzm'
pdf_sto_email = 'pzmastana@gmail.com'
roboto_font_path = os.path.abspath('fonts/Roboto-Regular.ttf')
roboto_bold_font_path = os.path.abspath('fonts/Roboto-Bold.ttf')
pdf_background_img_path = os.path.abspath('resources/img1.jpg')
pdf_logo_img_path = os.path.abspath('resources/pdf_logo.png')
pdf_background2_img_path = os.path.abspath('resources/paper-white-sheet-note.jpg')

# Объект бота
bot = Bot(token)
time_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
# Включаем логирование, чтобы не пропустить важные сообщения
logging.basicConfig(level=logging.INFO)
dp = Dispatcher(bot, storage=MemoryStorage())
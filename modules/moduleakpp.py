import os

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from buttons import *

from classes import Akpp
from config import akpp_zamena, bot, dp
from pdf_maker import make_pdf

######################################################### ЗАМЕНА МАСЛА ДВС #########################################################

from start_bot import *

@dp.message_handler(Text(equals="Замена масла в АКПП"), state='*')
async def akpp_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
            data["heading"] = message.text
    await message.answer("Введите пробег:", reply_markup=keyboard_cancel)
    await Akpp.next()

@dp.message_handler(state=Akpp.step1) # АКПП Ответ Пробег
async def akpp_calc(message: types.Message, state: FSMContext):
    o_probeg = message.text
    if o_probeg.isdigit():
        async with state.proxy() as data:
            data["probeg"] = message.text
        await message.answer("Введите название масла:")
        await Akpp.next()
    else:
        await message.answer("Введите пробег (Цифры):")


@dp.message_handler(state=Akpp.step2) # АКПП Ответ Масло
async def dvs_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["maslo"] = message.text
    await message.answer("Введите название маслянного фильтра:")
    await Akpp.next()


@dp.message_handler(state=Akpp.step3) # АКПП Ответ Фильтр
async def akpp_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["filter"] = message.text
    await message.answer("Введите марку и модель авто:")
    await Akpp.next()
    
@dp.message_handler(state=Akpp.step4) # АКПП Ответ Марка авто
async def akpp_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["marka"] = message.text
    await message.answer("Введите номер номер авто (ГРНЗ):")
    await Akpp.next()

@dp.message_handler(state=Akpp.step5) # АКПП Ответ Номер авто
async def akpp_calc(message: types.Message, state: FSMContext):
    o_num = message.text
    data = await state.get_data()
    o_heading = data.get("heading")
    o_probeg = data.get("probeg")
    o_maslo = data.get('maslo')
    o_mark = data.get('marka')
    o_filter = data.get('filter')

    do_probeg = o_probeg
    calc_o_probeg = int(o_probeg)+akpp_zamena # Замена каждые 40 000км
    o_probeg = str(calc_o_probeg)
    
    pdf = make_pdf({"Заголовок": o_heading,
                    "Пробег": do_probeg,
                    "Следующая замена": o_probeg,
                    "Название масла": o_maslo,
                    "Марка авто": o_mark,
                    "Название фильтра": o_filter,
                    "Номер авто": o_num})

    pdf.output(name = '%s.pdf' % o_num)
    # await message.answer("АКПП. След замена PDF")
    
    await bot.send_document(message.from_user.id, open('%s.pdf' % o_num, 'rb'), reply_markup=keyboard)
    os.remove('%s.pdf' % o_num)
    await state.reset_state(with_data=False)

######################################################### ЗАМЕНА МАСЛА АКПП #########################################################

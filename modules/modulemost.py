import os

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from buttons import *

from classes import Most
from config import bot, dp, most_zamena
from pdf_maker import make_pdf

######################################################### ЗАМЕНА МАСЛА Мостах #########################################################

from start_bot import *

@dp.message_handler(Text(equals="Замена масла в Мостах"), state='*')
async def most_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
            data["heading"] = message.text
    await message.answer("Введите пробег:", reply_markup=keyboard_cancel)
    await Most.next()

@dp.message_handler(state=Most.step1) # Мост Ответ Пробег
async def most_calc(message: types.Message, state: FSMContext):
    o_probeg = message.text
    if o_probeg.isdigit():
        async with state.proxy() as data:
            data["probeg"] = message.text
        await message.answer("Введите название масла:")
        await Most.next()
    else:
        await message.answer("Введите пробег (Цифры):")

@dp.message_handler(state=Most.step2) # Мост Ответ Масло
async def most_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["maslo"] = message.text
    await message.answer("Введите название маслянного фильтра:")
    await Most.next()

@dp.message_handler(state=Most.step3) # Мост Ответ Фильтр
async def most_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["filter"] = message.text
    await message.answer("Введите марку и модель авто:")
    await Most.next()
    
@dp.message_handler(state=Most.step4) # Мост Ответ Марка авто
async def most_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["marka"] = message.text
    await message.answer("Введите номер номер авто (ГРНЗ):")
    await Most.next()

@dp.message_handler(state=Most.step5) # Мост Ответ Номер авто + Сохранение и отправка в PDF
async def most_calc(message: types.Message, state: FSMContext):
    o_num = message.text
    data = await state.get_data()
    o_heading = data.get("heading")
    o_probeg = data.get("probeg")
    o_maslo = data.get('maslo')
    o_mark = data.get('marka')
    o_filter = data.get('filter')

    do_probeg = o_probeg
    calc_o_probeg = int(o_probeg)+most_zamena # Замена каждые 40000км
    o_probeg = str(calc_o_probeg)
    
    pdf = make_pdf({"Заголовок": o_heading,
                    "Пробег": do_probeg,
                    "Следующая замена": o_probeg,
                    "Название масла": o_maslo,
                    "Марка авто": o_mark,
                    "Название фильтра": o_filter,
                    "Номер авто": o_num})

    pdf.output(name = '%s.pdf' % o_num)
    # await message.answer("Мост. След замена PDF")
    
    await bot.send_document(message.from_user.id, open('%s.pdf' % o_num, 'rb'), reply_markup=keyboard)
    os.remove('%s.pdf' % o_num,)
    await state.reset_state(with_data=False)
    
######################################################### ЗАМЕНА МАСЛА Мостах #########################################################

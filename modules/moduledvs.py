import os

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from buttons import *

from classes import Dvs
from config import bot, dp, dvs_zamena
from pdf_maker import make_pdf

######################################################### ЗАМЕНА МАСЛА ДВС #########################################################

from start_bot import *

@dp.message_handler(Text(equals="Замена масла в ДВС"), state='*')
async def dvs_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
            data["heading"] = message.text
    await message.answer("Введите пробег:", reply_markup=keyboard_cancel)
    await Dvs.next()

@dp.message_handler(state=Dvs.step1) # ДВС Ответ Пробег
async def dvs_calc(message: types.Message, state: FSMContext):
    o_probeg = message.text
    if o_probeg.isdigit():
        async with state.proxy() as data:
            data["probeg"] = message.text
        await message.answer("Введите название масла:")
        await Dvs.next()
    else:
        await message.answer("Введите пробег (Цифры):")

@dp.message_handler(state=Dvs.step2) # ДВС Ответ Масло
async def dvs_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["maslo"] = message.text
    await message.answer("Введите название маслянного фильтра:")
    await Dvs.next()

@dp.message_handler(state=Dvs.step3) # ДВС Ответ Фильтр
async def dvs_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["filter"] = message.text
    await message.answer("Введите марку и модель авто:")
    await Dvs.next()
    
@dp.message_handler(state=Dvs.step4) # ДВС Ответ Марка авто
async def dvs_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["marka"] = message.text
    await message.answer("Введите номер номер авто (ГРНЗ):")
    await Dvs.next()

@dp.message_handler(state=Dvs.step5) # ДВС Ответ Номер авто + Сохранение и отправка в PDF
async def dvs_calc(message: types.Message, state: FSMContext):
    o_num = message.text
    data = await state.get_data()
    o_heading = data.get("heading")
    o_probeg = data.get("probeg")
    o_maslo = data.get('maslo')
    o_mark = data.get('marka')
    o_filter = data.get('filter')

    do_probeg = o_probeg
    calc_o_probeg = int(o_probeg)+dvs_zamena # Замена каждые 8 000км
    o_probeg = str(calc_o_probeg)
    
    pdf = make_pdf({"Заголовок": o_heading,
                    "Пробег": do_probeg,
                    "Следующая замена": o_probeg,
                    "Название масла": o_maslo,
                    "Марка авто": o_mark,
                    "Название фильтра": o_filter,
                    "Номер авто": o_num})

    pdf.output(name = '%s.pdf' % o_num)
    # await message.answer("ДВС. След замена PDF")
    
    await bot.send_document(message.from_user.id, open('%s.pdf' % o_num, 'rb'), reply_markup=keyboard)
    os.remove('%s.pdf' % o_num,)
    await state.reset_state(with_data=False)
    
######################################################### ЗАМЕНА МАСЛА ДВС #########################################################

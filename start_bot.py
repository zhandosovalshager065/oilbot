from aiogram import types
from aiogram.dispatcher import FSMContext
from config import dp
from aiogram.dispatcher.filters import Text
from buttons import *

@dp.message_handler(commands="start")
async def cmd_start(message: types.Message):
    await message.answer("Выберите замену масла или промывку", reply_markup=keyboard)

# Добавляем возможность отмены, если пользователь передумал заполнять
@dp.message_handler(state='*', commands='cancel')
@dp.message_handler(Text(equals='Отменить заполнение', ignore_case=True), state='*')
async def cancel_handler(message: types.Message, state: FSMContext):
    current_state = await state.get_state()
    if current_state is None:
        return

    await state.finish()
    await message.reply('Заполенение отменено', reply_markup=keyboard)
    await message.answer("Какой тип масла меняем?", reply_markup=keyboard)

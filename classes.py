from aiogram.dispatcher.filters.state import State, StatesGroup

class Dvs(StatesGroup): #Опрос ДВС
    step1 = State()
    step2 = State()
    step3 = State()
    step4 = State()
    step5 = State()

class Gur(StatesGroup):  # Опрос ГУР
    step1 = State()
    step2 = State()
    step3 = State()
    step4 = State()
    step5 = State()

class Akpp(StatesGroup): #Опрос АКПП
    step1 = State()
    step2 = State()
    step3 = State()
    step4 = State()
    step5 = State()

class Most(StatesGroup): #Опрос Мосты
    step1 = State()
    step2 = State()
    step3 = State()
    step4 = State()
    step5 = State()

class Inj(StatesGroup): #Опрос Инж
    step1 = State()
    step2 = State()
    step3 = State()
    # step4 = State()
    # step5 = State()